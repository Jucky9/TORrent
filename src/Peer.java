import java.io.IOException;

/**
 * Class containing a peer client and a peer server. The server is executed in a new thread.
 */
public class Peer {
    private PeerClient client;
    private PeerServer server;

    /**
     * Constructor
     * @param clientDirectory destination directory of the downloaded files
     * @param serverDirectory directory of the available files of the peer
     * @throws IOException
     */
    public Peer(String clientDirectory, String serverDirectory) throws IOException {
        if(clientDirectory.charAt(clientDirectory.length() - 1) != '/') {
            clientDirectory += '/';
        }
        if(serverDirectory.charAt(serverDirectory.length() - 1) != '/') {
            serverDirectory += '/';
        }
        OnionMaker onionMaker = new OnionMaker();
        server = new PeerServer(serverDirectory, onionMaker);
        server.startServer();
        client = new PeerClient(clientDirectory, onionMaker);
    }

    /**
     * Calls the main loop of the peer client.
     */
    public void mainLoop() {
        client.mainLoop();
    }
}
