import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.net.InetSocketAddress;


/**
 * Class representing the network which knows all the public servers (onion routing nodes and the tracker) on the network.
 * Its informations are accessible by every entity on the network.
 */
public class Network extends Server {

    /**
     * Request number corresponding to adding an onion routing node
     */
    public static final byte ADDNODE = 1;

    /**
     * Request number corresponding to getting the address and the key of the nodes and the tracker
     */
    public static final byte GETINFORMATION = 2;

    /**
     * Request number corresponding to adding the tracker
     */
    public static final byte ADDTRACKER = 3;

    /**
     * Contains the IP address and the RSA public key of the nodes in the onion path
     */
    private Map<InetSocketAddress, RSAPublicKey> nodesInformation;

    /**
     * Address of the tracker
     */
    private InetSocketAddress trackerAddress;

    /**
     * RSA public key of the tracker
     */
    private RSAPublicKey trackerPublicKey;

    /**
     * Indicates if the tracker is known
     */
    private boolean knowTracker;

    /**
     * Constructor
     * @param address network address
     * @throws IOException
     */
    public Network(InetSocketAddress address) throws IOException{
        super(address);
        nodesInformation = new ConcurrentHashMap<InetSocketAddress, RSAPublicKey>();
        knowTracker = false;
    }

    /**
     * Answers to a request made by an entity connected to the server
     * @param requestSocket entity that made the request
     */
    @Override
    protected void answerTheRequest(Socket requestSocket) {
        try {
            ObjectOutputStream os = new ObjectOutputStream(requestSocket.getOutputStream());
            ObjectInputStream is = new ObjectInputStream(requestSocket.getInputStream());
            byte typeRequest = is.readByte();
            switch (typeRequest) {
            case ADDNODE:
                addNode(os, is);
                break;
            case GETINFORMATION:
                sendNodesInformation(os, is);
                sendTrackerInformation(os, is);
                break;
            case ADDTRACKER:
                addTracker(os, is);
                break;
            default:
                System.out.println("Unknown request");
            }
            os.close();
            is.close();
            requestSocket.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds an onion routing node to the network
     * @param os stream to send information to the new node
     * @param is stream to receive information from the new node
     * @throws IOException
     */
    private void addNode(ObjectOutputStream os, ObjectInputStream is) throws IOException {
        try {
            InetSocketAddress address = (InetSocketAddress) is.readObject();
            RSAPublicKey key = (RSAPublicKey) is.readObject();
            nodesInformation.put(address, key);
            System.out.println("Node added to the network: " + address.toString());
            os.writeByte(0);
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends all the network information about the onion routing nodes on the network to an onion maker
     * @param os stream to send information to the onion maker
     * @param is stream to receive information from the onion maker
     * @throws IOException
     */
    private void sendNodesInformation(ObjectOutputStream os, ObjectInputStream is) throws IOException{
        os.writeInt(nodesInformation.size());
        for( InetSocketAddress address : nodesInformation.keySet()) {
            os.writeObject(address);
            os.writeObject(nodesInformation.get(address));
        }
        os.flush();
    }

    /**
     * Sends all the network information about the tracker to an onion maker
     * @param os stream to send information to the onion maker
     * @param is stream to receive information from the onion maker
     * @throws IOException
     */
    private void sendTrackerInformation(ObjectOutputStream os, ObjectInputStream is) throws IOException {
        synchronized(this) {
            try {
                while(!knowTracker) {
                    wait();
                }
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        os.writeObject(trackerAddress);
        os.writeObject(trackerPublicKey);
        os.flush();
        is.readByte();
    }

    /**
     * Adds a tracker to the network
     * @param os stream to send information to the tracker
     * @param is stream to receive information from the tracker
     * @throws IOException
     */
    private void addTracker(ObjectOutputStream os, ObjectInputStream is) throws IOException  {
        try {
            trackerAddress = (InetSocketAddress) is.readObject();
            trackerPublicKey = (RSAPublicKey) is.readObject();
            System.out.println("Tracker added to the network: " + trackerAddress);
            os.writeByte(0);
            synchronized(this) {
                knowTracker = true;
                notifyAll();
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
