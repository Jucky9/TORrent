import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectInput;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.ArrayList;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.math.BigInteger;

/**
 * Class representing a secure socker that is a socket which encrypts and decrypts its communications.
 * Uses AES128-CTR and RSAES-OAEP. RSAES-OAEP is used only for key exchanges.
 */
public class SecureSocket {

    /**
     * Socket
     */
    private Socket socket;

    /**
     * Output stream to send informations
     */
    private OutputStream os;

    /**
     * Input stream to receive informations
     */
    private InputStream is;

    /**
     * AES key of each socket with which the socket is communicating
     */
    private List<byte[]> keys;

    /**
     * AES128-CTR counter of each socket with which the socket is communicating
     */
    private List<BigIntegerPointer> ctrs;

    /**
     * RSAES-OAEP used for encryption
     */
    private RSAES_OAEP rsa;

    /**
     * AES128-CTR used for encryption
     */
    private AES128_CTR aes128;

    /**
     * Constructor using an existing socket
     * @param socket socket to secure
     * @throws IOException
     */
    public SecureSocket(Socket socket) throws IOException {
        this.socket = socket;
        os = socket.getOutputStream();
        is = socket.getInputStream();
        keys = new ArrayList<byte[]>();
        ctrs = new ArrayList<BigIntegerPointer>();
        aes128 = new AES128_CTR();
    }

    /**
     * Constructor using an address
     * @param address address of the secure socket to create
     * @throws IOException
     */
    public SecureSocket(InetSocketAddress address) throws IOException {
        this(new Socket(address.getAddress(), address.getPort()));
    }

    public void setRsa(RSAES_OAEP rsa) {
        this.rsa = rsa;
    }

    /**
     * Sends a RSAES-OAEP encrypted message. That message is encrypted with AESCTR-128 if the socket is already communicating with
     * at least one other socket (see encrypt function).
     * @param message message to send
     * @param rsaPublicKey RSA public key used for encryption
     * @throws IOException
     */
    public void sendRSAEncryption(byte[] message, RSAPublicKey rsaPublicKey) throws IOException{
        byte[] RSAEncrypt = rsa.encrypt(message, rsaPublicKey);
        send(encrypt(RSAEncrypt));
    }

    /**
     * Receives a message and decrypts it with RSAES-OAEP
     * @return the decrypted message
     * @throws IOException
     */
    public byte[] receiveRSADecrypt() throws IOException{
        return rsa.decrypt(receive());
    }

    /**
     * Receives a AES128-CTR key and a AES128-CTR counter
     * @throws IOException
     */
    public void receiveAES128KeyCtr()throws IOException {
        byte[] typeKeyCtr = receiveRSADecrypt();
        byte[] key = Arrays.copyOfRange(typeKeyCtr, 1, 17);
        byte[] ctr = Arrays.copyOfRange(typeKeyCtr, 17, typeKeyCtr.length);
        addAES128KeyCtr(key, new BigInteger(ctr));
    }

    /**
     * Receives AES128-CTR keys and their matching counters from another socket
     * @param knowKeyCtr key counter known
     */
    public void receiveKeysCtrsFrom(SecureSocket knowKeyCtr) {
        for( byte[] key : knowKeyCtr.keys) {
            keys.add(key);
        }
        for(BigIntegerPointer ctr : knowKeyCtr.ctrs) {
            ctrs.add(ctr);
        }
    }

    /**
     * Saves a AES128-CTR key and its matching counter
     * @param key key to save
     * @param ctr counter to save
     */
    public void addAES128KeyCtr(byte[] key, BigInteger ctr) {
        keys.add(0, key);
        ctrs.add(0, new BigIntegerPointer(ctr));
    }

    /**
     * Sends the size of a byte array and the byte array itself
     * @param toSend byte array to send
     * @throws IOException
     */
    private void send(byte[] toSend)throws IOException{
        os.write(intToByte(toSend.length)); // Size of the message
        os.write(toSend, 0, toSend.length);
        os.flush();
    }

    /**
     * Receives the size of a byte array and the byte array itself
     * @throws IOException
     */
    private byte[] receive()throws IOException{
        byte[] sizeBits = new byte[4]; // Size of the message
        if(is.read(sizeBits, 0, 4) != -1) {
            int size = byteToInt(sizeBits);
            return receiveExactlyByte(size);
        }
        return null;
    }

    /**
     * Sends a serialized object
     * @param toSend object to send
     * @throws IOException
     */
    public void sendObject(Object toSend) throws IOException {
        byte[] byteToSend = objectToByte(toSend);
        byteToSend = encrypt(byteToSend);
        send(byteToSend);
    }

    /**
     * Receives a serialized object and deserializes it
     * @return the deserialized object
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Object receiveObject() throws IOException, ClassNotFoundException {
        byte[] objectInByte = receive();
        objectInByte = decrypt(objectInByte);
        return byteToObject(objectInByte);
    }

    /**
     * Sends an integer
     * @param toSend integer to send
     * @throws IOException
     */
    public void sendInt(int toSend) throws IOException {
        send(encrypt(toSend));
    }

    /**
     * Receives an integer
     * @return the received integer
     * @throws IOException if there is a problem with the client input
     */
    public int receiveInt() throws IOException {
        return byteToInt(decrypt(receive()));
    }

    /**
     * Sends a string
     * @param toSend string to send
     * @throws IOException
     */
    public void sendString(String toSend) throws IOException {
        sendObject(toSend);
    }

    /**
     * Receives a string
     * @return the received string
     * @throws IOException
     */
    public String receiveString() throws IOException {
        try {
            return (String) receiveObject();
        }
        catch (ClassNotFoundException e) {
            return null;
        }
    }

    /**
     * Sends a byte array
     * @param toSend byte array to send
     * @param needEncrypt indicates if a decryption of the array to send is needed
     * @throws IOException
     */
    public void sendByte(byte[] toSend, boolean needEncrypt) throws IOException {
        if(needEncrypt) {
            byte [] criptetToSend = encrypt(toSend);
            send(criptetToSend);
        }
        else {
            send(toSend);
        }
    }

    /**
     * Receives a byte array
     * @param needDecrypt indicates if a decryption of the received array is needed
     * @return the received byte array
     * @throws IOException
     */
    public byte[] receiveByte(boolean needDecrypt) throws IOException {
        byte[] buffer = receive();
        if(buffer != null && needDecrypt) {
            buffer = decrypt(buffer);
        }
        return buffer;
    }

    /**
     * Receives exactly a certain number of bytes
     * @param sizebyteToReceive number of bytes to receive
     * @return byteToReceive
     * @throws IOException
     */
    private byte[] receiveExactlyByte(int sizebyteToReceive) throws IOException{
        byte[] byteToReceive = new byte[sizebyteToReceive];
        int size = 0;
        while (size < sizebyteToReceive ) {
            size += is.read(byteToReceive, size, sizebyteToReceive - size);
        }
        return byteToReceive;
    }

    /**
     * Serializes an object into byte
     * @param toByte object to serialize into a byte array
     * @throws IOException
     */
    private byte[] objectToByte(Object toByte) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(toByte);
        out.flush();
        byte[] temp = bos.toByteArray();
        bos.close();
        out.close();
        return temp;
    }

    /**
     * Deserializes a byte array into an object
     * @param toByte object to serialize into a byte array
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private Object byteToObject(byte[] toObject) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(toObject, 0, toObject.length);
        ObjectInput in = new ObjectInputStream(bis);
        Object temp = in.readObject();
        in.close();
        return temp;
    }

    /**
     * Encrypts an integer
     * @param val integer to encrypt
     */
    private byte[] encrypt(int val) {
        return encrypt(intToByte(val));
    }

    /**
     * Converts an integer into a byte array
     * @param val value to convert
     */
    private byte[] intToByte(int val) {
        return ByteBuffer.allocate(4).putInt(val).array();
    }

    /**
     * Converts a byte array into an integer
     * @param bytes byte array to convert
     */
    private int byteToInt(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }

    /**
     * Encrypts a byte array with the key of each socket with which the socket is communicating. If the socket is not yet
     * communicating with another one, the byte array is returned as it was received.
     * @param toEncrypt byte array to encrypt
     * @return toEncrypt encrypted byte array
     */
    private byte[] encrypt(byte[] toEncrypt) {
        for(int i = 0 ; i < keys.size() ; i++ ) {
            toEncrypt = aes128.encrypt(toEncrypt, keys.get(i), ctrs.get(i));
        }
        return toEncrypt;
    }

    /**
     * Decrypts a byte array with the key of each socket with which the socket is communicating.
     * @param toDecrypt byte array to decrypt
     * @return toDecrypt decrypted byte array
     */
    private byte[] decrypt(byte[] toDecrypt) {
        for(int i = keys.size() - 1; i > -1 ; i-- ) {
            toDecrypt = aes128.decrypt(toDecrypt, keys.get(i), ctrs.get(i));
        }
        return toDecrypt;
    }

    /**
     * Closes the socket, the input and the output streams.
     */
    public void close() {
        try {
            socket.close();
        }
        catch(IOException e) {}
        try {
            is.close();
        }
        catch(IOException e) {}
        try {
            os.close();
        }
        catch(IOException e) {}
    }
}
