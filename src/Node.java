import java.io.IOException;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Arrays;
import java.nio.ByteBuffer;
import java.math.BigInteger;

/**
 * Class representing an onion routing node
 */
public class Node extends PublicServer {

    /**
     * Maps the pending meetings keys and their matching server
     */
    private Map<BigInteger, SecureSocket> meeting;

    /**
     * Request number corresponding to the creation of a tunnel
     */
    public static final byte TUNNEL = 0;

    /**
     * Request number corresponding to the creation of a meeting point in this node
     */
    public static final byte CREATEMEETING = 1;

    /**
     * Request number corresponding to the join of a peer client to a meeting in this node
     */
    public static final byte JOINMEETING = 2;

    /**
     * Constructor
     * @param address address of the onion routing node
     * @throws IOException
     */
    public Node(InetSocketAddress address) throws IOException {
        super(address);
        meeting = new ConcurrentHashMap<BigInteger, SecureSocket>();
    }

    /**
     * Returns the type of the request to the network. See Network class attributes.
     */
    @Override
    protected byte getCaseNotify() {
        return Network.ADDNODE;
    }

    /**
     * Answers to a request made by an entity connected to the server
     * @param requestSocket entity that made the request
     */
    @Override
    protected void answerTheRequest(Socket requestSocket) {
        try {
            SecureSocket first = new SecureSocket(requestSocket);
            first.setRsa(getRSA());
            try {
                byte[] initializationMessage = first.receiveRSADecrypt();
                byte typeRequest = initializationMessage[0];
                byte[] key = Arrays.copyOfRange(initializationMessage, 1, 17);
                byte[] ctr;
                switch(typeRequest) {
                case TUNNEL:
                    ctr = Arrays.copyOfRange(initializationMessage, 17, initializationMessage.length);
                    caseTunnel(first, key, ctr);
                    break;
                case CREATEMEETING:
                    ctr = Arrays.copyOfRange(initializationMessage, 17, initializationMessage.length);
                    caseCreateMeetingPoint(first, key, ctr);
                    break;
                case JOINMEETING:
                    caseJoinMeetingPoint(first, key);
                    break;
                default:
                    System.out.println("Unknow request");
                    first.close();
                }
            }
            catch(IOException e) {
                e.printStackTrace();
                first.close();
            }
            catch(ClassNotFoundException e) {
                e.printStackTrace();
                first.close();
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a tunnel between two entities that are communicating with this onion routing node
     * @param entity communicating with this onion routing node
     * @param key AES key between first and this onion routing node
     * @param ctr AES128-CTR counter between first and this onion routing node
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void caseTunnel(SecureSocket first, byte[] key, byte[] ctr) throws IOException, ClassNotFoundException {
        first.addAES128KeyCtr(key, new BigInteger(ctr));
        InetSocketAddress nextAddress = (InetSocketAddress) first.receiveObject();
        System.out.println("The node after me in the new tunnel is: " + nextAddress.toString());
        SecureSocket second = new SecureSocket(nextAddress);
        second.receiveKeysCtrsFrom(first); // Another entity communicating with this onion routing node
        new Tunnel(first, second);
    }

    /**
     * Creates a meeting point between a peer server and a peer client
     * @param first socket to communicate with the node leading to the peer server from this onion routing node
     * @param key AES key between the peer server asking for the meeting and this onion routing node
     * @param ctr AES128-CTR counter between the peer server asking for the meeting and this onion routing node
     * @throws IOException
     */
    private void caseCreateMeetingPoint(SecureSocket first, byte[] key, byte[] ctr) throws IOException {
        System.out.println("The peer server arrived at the meeting");
        first.addAES128KeyCtr(key, new BigInteger(ctr));
        BigInteger meetingKey = new BigInteger(first.receiveByte(true));//convert to BigInteger because byte[] can't be map key
        synchronized(this) {
            while(meeting.containsKey(meetingKey)) {// should never happen: 2 peer should have choose same node and same key
                try {
                    wait();
                }
                catch(InterruptedException e) {
                }
            }
            meeting.put(meetingKey, first);
            notifyAll();
        }
    }

    /**
     * Joins two a peer server and a peer client in a meeting in this onion routing node
     * @param first socket to communicate with the node leading to the peer client from this onion routing node
     * @param key AES key needed to join the meeting
     * @throws IOException
     */
    private void caseJoinMeetingPoint(SecureSocket first, byte[] key) throws IOException {
        System.out.println("The peer client arrived at the meeting");
        BigInteger meetingKey = new BigInteger(key);//convert to BigInteger because byte[] can't be map key
        synchronized(this) {
            while(!meeting.containsKey(meetingKey)) {
                try {
                    wait();
                }
                catch(InterruptedException e) {
                }
            }
            SecureSocket second = meeting.get(meetingKey);
            new Tunnel(second, first);
            meeting.remove(meetingKey);
        }
    }
}
