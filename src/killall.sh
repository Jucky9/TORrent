#!/bin/bash
old=$IFS
IFS=$'\n'
for javaid in $(ps aux | grep java | grep Launcher)
do
    IFS=$old
    i=0
    for val in $javaid
    do
        i=$i+1
        if (( "$i" == "2" ))
        then
            kill -9 $val
        fi  
    done
    IFS=$'\n'
done
