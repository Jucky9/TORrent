import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Class launching the tracker
 */
public class TrackerLauncher {

    /**
     * Launches the tracker
     * @param args tracker port, tracker address, network address
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        if(args.length >= 3) {
            NetworkAddress.setNetworkAddress(args[2]);
            Tracker tracker = new Tracker(new InetSocketAddress(args[1], Integer.valueOf( args[0])));
            tracker.listen();
        }
        else {
            System.out.println("Arguments error: The first argument is the port of the tracker. " +
            "The second argument is the hostname of the tracker. " +
            "The third argument is the network hostname.");
        }
    }
}
