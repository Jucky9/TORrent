import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.File;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.math.BigInteger;

/**
 * Class that implements the server side of a peer
 */
public class PeerServer {

    /**
     * Maximum number of bytes sent by a peer at once
     */
    private int MAXBYTESEND = 495;

    /**
     * Directory of the available files of the peer
     */
    private String directory;

    /**
     * Filenames of the available files of the peer
     */
    private List<String> files;

    /**
     * Socket that is permanently connected to the tracker
     */
    private SecureSocket trackerConnection;

    /**
     * Onion maker for the creation of an onion path
     */
    private OnionMaker onionMaker;

    /**
     * Constructor
     * @param directory directory of the available files of the peer
     * @param onionMaker used for the creation of an onion path
     * @throws IOException
     */
    public PeerServer(String directory, OnionMaker onionMaker) throws IOException {
        this.directory = directory;
        saveAvailableFilenames();
        this.onionMaker = onionMaker;
        notifyTracker();
    }

    /**
     * Starts the peer's server side
     * @throws IOException
     */
    public void startServer() throws IOException {
        Thread thread = new Thread(() -> listen());
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Connects to the tracker and sends it the available files of the peer
     * @throws IOException
     */
    public void notifyTracker() throws  IOException{
        System.out.println("Connecting to the tracker...");
        trackerConnection = onionMaker.connectTracker();
        trackerConnection.sendInt(Tracker.ADDPEERSERVER);
        trackerConnection.sendInt(files.size());
        for(int i = 0 ; i < files.size() ; i++ ) {
            trackerConnection.sendString(files.get(i));
        }
    }

    /**
     * Listens for new file requests
     */
    private void listen() {
        try {
            while(true) {
                String filename = trackerConnection.receiveString();
                byte[] sharedKey = trackerConnection.receiveByte(true);
                byte[] ctr = trackerConnection.receiveByte(true);
                byte[] meetingKey = AES128_CTR.generateKey();
                trackerConnection.sendByte(meetingKey, true);
                InetSocketAddress meetingPoint = onionMaker.getRandomNode();
                trackerConnection.sendObject(meetingPoint);
                Thread senderFile = new Thread(()->sendFile(filename, sharedKey, meetingKey, meetingPoint, new BigInteger(ctr)));
                senderFile.setDaemon(true);
                senderFile.start();
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the filenames of the available files of the peer
     */
    private void saveAvailableFilenames() {
        files = new ArrayList<String>();
        File directory = new File(this.directory);
        File[] listOfFiles = directory.listFiles();
        System.out.println();
        System.out.println("Files available on this server:");
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("    " + listOfFiles[i].getName());
                files.add(listOfFiles[i].getName());
            }
        }
        System.out.println();
    }

    /**
     * Sends a requested file
     * @param filename filename of the requested file
     * @param sharedKey AES key shared with the peer who requested the file
     * @param meetingKey Key needed to join the meeting with the peer who requested the file
     * @param meetingPoint onion routing node where the meeting with the peer who requested the file takes place
     * @param ctr define the ctr
     */
    private void sendFile(String fileName, byte[] sharedKey, byte[] meetingKey, InetSocketAddress meetingPoint, BigInteger ctr) {
        try {
            SecureSocket socket = onionMaker.createMeeting(meetingPoint, meetingKey);
            socket.addAES128KeyCtr(sharedKey, ctr);
            Path path = Paths.get(directory + fileName);
            byte[] data = Files.readAllBytes(path);
            socket.sendInt(data.length);
            int alreadySend = 0;
            while(alreadySend < data.length) {
                int toSendSize = Math.min(MAXBYTESEND, data.length - alreadySend);
                byte[] dataToSend = Arrays.copyOfRange(data, alreadySend, alreadySend + toSendSize);
                socket.sendByte(dataToSend, true);
                alreadySend += toSendSize;
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
}
